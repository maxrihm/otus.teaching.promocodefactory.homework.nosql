using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class EmployeeMongoRepository : IRepository<Employee>        
    {
        private readonly MongoClient _client;

        private readonly IMongoDatabase _database;

        private IMongoCollection<Employee> _employeeCollection;

        private IRepository<Role> _roleRepository;

        public EmployeeMongoRepository(MongoClient client, IRepository<Role> roleRepository)
        {
            _client = client;
            _database = _client.GetDatabase("promocodefactory");
            _employeeCollection = _database.GetCollection<Employee>("employees");
            _roleRepository = roleRepository;
        }

        public Task AddAsync(Employee entity)
        {
            return _employeeCollection.InsertOneAsync(entity);
        }

        public Task DeleteAsync(Employee entity)
        {
            return _employeeCollection.DeleteOneAsync(entity.ToJson());
        }
        public Task UpdateAsync(Employee entity)
        {
            var filter = new BsonDocument { { "_id", $"{entity.Id}" } };            
            return _employeeCollection.ReplaceOneAsync(filter, entity);
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
           return await _employeeCollection.Find("{}").ToListAsync();                        
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            var filter = new BsonDocument { { "_id", id.ToString() } };
            var employee = await _employeeCollection.Find(filter).FirstAsync();                        
            if (employee == null) {
                return null;
            }
            var roleCollection = await _roleRepository.GetAllAsync();            
            var role = roleCollection.FirstOrDefault(r => employee.Role.Id == r.Id);
            employee.Role = role;
            return employee;
        }

        public async Task<Employee> GetFirstWhere(Expression<Func<Employee, bool>> predicate)
        {
            return await _employeeCollection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _employeeCollection.Find(x=> ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<Employee>> GetWhere(Expression<Func<Employee, bool>> predicate)
        {
            return await _employeeCollection.Find(predicate).ToListAsync();
        }

    }
}
