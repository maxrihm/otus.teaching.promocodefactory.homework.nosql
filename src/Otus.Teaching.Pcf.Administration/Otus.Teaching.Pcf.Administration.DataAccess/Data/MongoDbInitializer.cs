using System;
using System.Diagnostics;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private MongoClient _client;

        public MongoDbInitializer(MongoClient client)
            => _client = client;

        public async void InitializeDb()
        {
            IMongoDatabase database = _client.GetDatabase("promocodefactory");
            try
            {
                await database.CreateCollectionAsync("employees");
                await database.CreateCollectionAsync("roles");
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
            IMongoCollection<Employee> employees = database.GetCollection<Employee>("employees"); 
            IMongoCollection<Role> roles = database.GetCollection<Role>("roles");
                               
            await roles.InsertManyAsync(FakeDataFactory.Roles);
            await employees.InsertManyAsync(FakeDataFactory.Employees);
        }

    }
}
